const express = require('express');
const bodyParser = require('body-parser');


//const router = require('./components/message/network');
const router = require('./network/routes'); 

var app = express(); 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
//app.use(router);
router(app);


app.use('/app', express.static('public'));

// router.post('/message', (req, res)=>{
//   res.send('Nuevo mensaje agregado de manera correcta');
// });

// app.use('/', (req, res)=>{
//   res.send('Hola mundo desde node con express, usando una arrow function');
// });

app.listen(3000);
console.log('La aplicacion está escuchando en http://localhost:3000');
