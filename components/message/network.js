const express = require('express');
const multer = require('multer');
const router = express.Router();
const response = require('../../network/response');
const controller = require('./controller');


router.get('/', (req, res) => {
  const filterMessages = req.query.user || null;
  controller.getMessages(filterMessages)
    .then((messageList) =>{
      response.success(req, res, messageList, 200);
    })
    .catch((err) => {
      response.error(req, res, 'Unexpected error', 500, err);
    });
});

router.post('/', (req, res) => {
  controller.addMessage(req.body.user, req.body.message)
    .then((fullMessage)=>{
      response.success(req, res, fullMessage, 201);
    })
    .catch((err)=>{
      console.error(err);
      response.error(req, res, 'Informacion invalida', 400, 'Error en el controlador.');
    });
});

router.patch('/:id', function(req, res){
  controller.updateMessage(req.params.id, req.body.message)
    .then((data) => {
      response.success(req, res, data, 200);
    })
    .catch((err)=>{
      response.error(req, res, 'Error interno', 500, err);
    });
});

router.delete('/:id', function(req, res){
  controller.deleteMessage(req.params.id)
    .then(()=>{
      response.success(req, res, `Usuario ${req.params.id} eliminado`, 200);
    })
    .catch((err)=>{
      response.error(req, res, 'Error interno', 500, err);
    });
});

module.exports = router;