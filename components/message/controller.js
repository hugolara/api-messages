//Aqui va la logica o las funciones que tendra la aplicacion de mensajeria
const store = require('./store');

function addMessage(user, message){

  return new Promise((resolve, reject) => {
    if(!user || !message){
      console.error('[messageController] No hay usuario o mensaje');
      reject('Datos incorrectos');
      return false;
    }
    const fullMessage = {
      user: user,
      message: message,
      date: new Date()
    };
    store.add(fullMessage);
    resolve(fullMessage);
  });
}

function getMessages(filterUser){
  return new Promise((resolve, reject)=>{
    resolve(store.list(filterUser));
  });
}

function updateMessage(id, message){
  return new Promise((resolve, reject)=>{
    if(!id || !message){
      reject('Invalid data');
      return false;
    }
    resolve(store.updateText(id, message));
  });
}

function deleteMessage(id){
  return new Promise((resolve, reject)=>{
    if(!id){
      reject('id invalido');
      return false;
    }
    store.remove(id)
      .then(()=>{
        resolve();
      })
      .catch(err =>{
        reject(err);
      }) 
  });
}

module.exports =  {
  addMessage,
  getMessages,
  updateMessage,
  deleteMessage 
};