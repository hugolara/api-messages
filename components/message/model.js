//Este archivo va a contener el modelo de la base de datos
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mySchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  message: {
    type: String,
    require: true
  },
  date: Date
});

const model = mongoose.model('messages', mySchema);
module.exports = model;